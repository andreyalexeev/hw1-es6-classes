"use strict";

// task 1

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}

const john = new Employee("John Doe", 30, 50000);
console.log(`Ім'я: ${john.name}`);
console.log(`Вік: ${john.age}`);
console.log(`Зарплата: $${john.salary}`);

// task 2

// class Employee {
//     constructor(name, age, salary) {
//         this._name = name;
//         this._age = age;
//         this._salary = salary;
//     }

//     get name() {
//         return this._name;
//     }

//     set name(newName) {
//         this._name = newName;
//     }

//     get age() {
//         return this._age;
//     }

//     set age(newAge) {
//         if (newAge >= 0) {
//             this._age = newAge;
//         } else {
//             console.error("Вік не може бути від'ємним!");
//         }
//     }

//     get salary() {
//         return this._salary;
//     }

//     set salary(newSalary) {
//         if (newSalary >= 0) {
//             this._salary = newSalary;
//         } else {
//             console.error("Зарплата не може бути від'ємною!");
//         }
//     }
// }

// const john = new Employee("John Doe", 30, 50000);
// console.log(`Ім'я: ${john.name}`);
// console.log(`Вік: ${john.age}`);
// console.log(`Зарплата: $${john.salary}`);

// john.name = "Jane Smith";
// john.age = 35;
// john.salary = 55000;

// console.log(`Нове ім'я: ${john.name}`);
// console.log(`Новий вік: ${john.age}`);
// console.log(`Нова зарплата: $${john.salary}`);

// task 3

// class Employee {
//     constructor(name, age, salary) {
//         this._name = name;
//         this._age = age;
//         this._salary = salary;
//     }

//     get name() {
//         return this._name;
//     }
//     set name(newName) {
//         this._name = newName;
//     }

//     get age() {
//         return this._age;
//     }
//     set age(newAge) {
//         if (newAge >= 0) {
//             this._age = newAge;
//         } else {
//             console.error("Вік не може бути від'ємним!");
//         }
//     }

//     get salary() {
//         return this._salary;
//     }
//     set salary(newSalary) {
//         if (newSalary >= 0) {
//             this._salary = newSalary;
//         } else {
//             console.error("Зарплата не може бути від'ємною!");
//         }
//     }
// }

// class Programmer extends Employee {
//     constructor(name, age, salary, lang) {
//         super(name, age, salary);
//         this.lang = lang;
//     }
// }

// const alice = new Programmer("Alice Smith", 28, 60000, ["JavaScript", "Python", "Java"]);
// console.log(`Ім'я: ${alice.name}`);
// console.log(`Вік: ${alice.age}`);
// console.log(`Зарплата: $${alice.salary}`);
// console.log(`Мови програмування: ${alice.lang.join(", ")}`);

// task 4

// class Employee {
//     constructor(name, age, salary) {
//         this._name = name;
//         this._age = age;
//         this._salary = salary;
//     }

//     get name() {
//         return this._name;
//     }
//     set name(newName) {
//         this._name = newName;
//     }

//     get age() {
//         return this._age;
//     }
//     set age(newAge) {
//         if (newAge >= 0) {
//             this._age = newAge;
//         } else {
//             console.error("Вік не може бути від'ємним!");
//         }
//     }

//     get salary() {
//         return this._salary;
//     }
//     set salary(newSalary) {
//         if (newSalary >= 0) {
//             this._salary = newSalary;
//         } else {
//             console.error("Зарплата не може бути від'ємною!");
//         }
//     }
// }

// class Programmer extends Employee {
//     constructor(name, age, salary, lang) {
//         super(name, age, salary);
//         this.lang = lang;
//     }

//     get salary() {
//         return this._salary * 3;
//     }
// }

// const alice = new Programmer("Alice Smith", 28, 60000, ["JavaScript", "Python", "Java"]);
// console.log(`Ім'я: ${alice.name}`);
// console.log(`Вік: ${alice.age}`);
// console.log(`Зарплата: $${alice.salary}`);
// console.log(`Мови програмування: ${alice.lang.join(", ")}`);

// task 5

// const programmer1 = new Programmer("Alice Smith", 28, 60000, ["JavaScript", "Python"]);
// console.log(`Ім'я: ${programmer1.name}`);
// console.log(`Вік: ${programmer1.age}`);
// console.log(`Зарплата: $${programmer1.salary}`);
// console.log(`Мови програмування: ${programmer1.lang.join(", ")}`);

// // Створення об'єкта Programmer зі списком мов Java та C++
// const programmer2 = new Programmer("Bob Johnson", 35, 75000, ["Java", "C++"]);
// console.log(`Ім'я: ${programmer2.name}`);
// console.log(`Вік: ${programmer2.age}`);
// console.log(`Зарплата: $${programmer2.salary}`);
// console.log(`Мови програмування: ${programmer2.lang.join(", ")}`);

// // Створення об'єкта Programmer зі списком мов Ruby та PHP
// const programmer3 = new Programmer("Carol Brown", 42, 90000, ["Ruby", "PHP"]);
// console.log(`Ім'я: ${programmer3.name}`);
// console.log(`Вік: ${programmer3.age}`);
// console.log(`Зарплата: $${programmer3.salary}`);
// console.log(`Мови програмування: ${programmer3.lang.join(", ")}`);






